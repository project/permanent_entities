<?php

namespace Drupal\permanent_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Permanent Entity type entities.
 */
interface PermanentEntityTypeInterface extends ConfigEntityInterface {

  /**
   * Wheter the entities of this type provide a response to the canonical route.
   *
   * @return boolean
   *   If TRUE entities of this type will return content when visiting the
   *   canonical route: /permanent_entities/[MACHINE_NAME]
   */
  public function fullModeAvailable();

  /**
   * Sets the full mode avaialble property.
   *
   * @param boolean $available
   *   A boolean value indicating if full mode is available or not.
   */
  public function setfullModeAvailable(bool $available);

}
