<?php

namespace Drupal\permanent_entities\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Permanent Entity entities.
 */
class PermanentEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}
