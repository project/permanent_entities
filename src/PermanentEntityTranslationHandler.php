<?php

namespace Drupal\permanent_entities;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for permanent_entity.
 */
class PermanentEntityTranslationHandler extends ContentTranslationHandler {

}
