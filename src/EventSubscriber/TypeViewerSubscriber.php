<?php

namespace Drupal\permanent_entities\EventSubscriber;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\permanent_entities\Entity\PermanentEntityType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Event\RequestEvent;


/**
 * Class TypeViewerSubscriber.
 */
class TypeViewerSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a new TypeViewerSubscriber object.
   */
  public function __construct(CurrentRouteMatch $current_route_match) {
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST] = ['onRequest'];

    return $events;
  }

  /**
   * This method is called when the KernelEvents::REQUEST is dispatched.
   *
   * @param \Symfony\Contracts\EventDispatcher\Event $event
   *   The dispatched event.
   */
  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();
    $route_name = $this->currentRouteMatch->getRouteName();
    if ($route_name === 'entity.permanent_entity.canonical') {
      $entity = $this->currentRouteMatch->getParameter('permanent_entity');
      $entity_type = PermanentEntityType::load($entity->bundle());
      if (!$entity_type->fullModeAvailable()) {
        throw new NotFoundHttpException();
      }
    }
  }


}
