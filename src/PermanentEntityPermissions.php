<?php

namespace Drupal\permanent_entities;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\permanent_entities\Entity\PermanentEntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions of the permanent_entity module.
 *
 * @see permanent_entities.permissions.yml
 */
class PermanentEntityPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TaxonomyPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Get permanent_entity permissions.
   *
   * @return array
   *   Permissions array.
   */
  public function permissions() : array{
    $permissions = [];
    foreach (PermanentEntityType::loadMultiple() as $permanent_entity_type) {
      $permissions += $this->buildPermissions($permanent_entity_type);
    }
    return $permissions;
  }

  /**
   * Builds a standard list of permanent_entity permissions for a given type.
   *
   * @param \Drupal\permanent_entities\Entity\PermanentEntityType $permanent_entity_type
   *   The permanent_entity_type.
   *
   * @return array
   *   An array of permission names and descriptions.
   */
  protected function buildPermissions(PermanentEntityType $permanent_entity_type): array {
    $id = $permanent_entity_type->id();
    $args = ['%permanent_entity_type' => $permanent_entity_type->label()];

    return [
      "edit permanent entities of type $id" => [
        'title' => $this->t('%permanent_entity_type: Edit permanent entities', $args),
      ],
    ];
  }

}
