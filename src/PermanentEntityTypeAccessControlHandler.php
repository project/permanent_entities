<?php

namespace Drupal\permanent_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Permanent Entity types.
 *
 * @see \Drupal\permanent_entities\Entity\PermanentEntityType.
 */
class PermanentEntityTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\permanent_entities\Entity\PermanentEntityTypeInterface $entity */
    if ($operation !== 'delete') {
      return parent::checkAccess($entity, $operation, $account);
    }
    $query = \Drupal::entityTypeManager()->getStorage('permanent_entity')->getQuery()->accessCheck(TRUE);
    $ids = $query->condition('type', $entity->id())->execute();

    if (!empty($ids)) {
      // Prevent deleting the bundle if there are instances of it created.
      return AccessResult::forbidden()->addCacheableDependency($entity);
    }
    return AccessResult::neutral();
  }

}
