<?php

/**
 * @file
 * Contains drush integration.
 */

namespace Drupal\permanent_entities\Commands;

use Drupal\permanent_entities\Entity\PermanentEntity;
use Drupal\permanent_entities\Entity\PermanentEntityType;
use Drush\Commands\DrushCommands;

/**
 * Class PermanentEntitiesCommands.
 */
class PermanentEntitiesCommands extends DrushCommands {

  /**
   * Creates a permanent entity.
   *
   * @param string $type
   *   The permanent entity type.
   * @param string $id
   *   The permanent entity id.
   * @param string $label
   *   The permanent entity label.
   *
   * @command permanent-entity:create
   * @aliases pec
   * @usage drush pec planet jupiter Jupiter
   *   Creates a planet with id jupiter and label Jupiter.
   * @validate-module-enabled permanent_entities
   */
  public function create($type, $id, $label) {
    if (!PermanentEntityType::load($type)) {
      $this->logger()->error(dt('The permanent entity type "@type" does not exist.', ['@type' => $type]));
      return;
    }

    if (PermanentEntity::load($id)) {
      $this->logger()->error(dt('The permanent entity "@id" already exists.', ['@id' => $id]));
      return;
    }

    PermanentEntity::create([
      'label' => $label,
      'id' => $id,
      'type' => $type,
    ])->save();

    $this->output()->writeln(dt('Permanent entity created.'));
  }
}
