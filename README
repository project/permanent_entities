Permanent Entities:

Project documentation is available here:
https://www.drupal.org/docs/8/modules/permanent-entities

Project description:

The Permanent Entities module provides a new type of entities that cannot be
created or deleted using the Drupal UI. This is valid also for site
administrators with full privileges.

Permanent entities can only be created and deleted by code (using hook_update_N
or via drush commands). Edit permissions are available per bundle.

The main use case of this module is to have regular entities that you don't want
to be deleted by error by a content administrator. Also this usually applies for
a set of content that rarely varies over time. Like the planets of the solar
system, the districts of a city, weather season, etc.

Users will still be able to edit and translate this entities. But they won't be
able to delete them, or create new ones using an user interface.
