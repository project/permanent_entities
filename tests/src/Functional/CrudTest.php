<?php

namespace Drupal\Tests\permanent_entities\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\permanent_entities\Entity\PermanentEntity;
use Drupal\permanent_entities\Entity\PermanentEntityType;

/**
 * Checks that is impossible create or delete permanent entities from the UI.
 *
 * @group permanent_entities
 */
class CrudTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['permanent_entities'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->admin = $this->drupalCreateUser([], 'superadmin', TRUE);
    $this->drupalLogin($this->admin);
  }

  /**
   * Test that is not possible to create or delete permanent entities.
   */
  public function testNoAddOrDelete() {
    $this->drupalGet(Url::fromRoute('entity.permanent_entity.collection'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('Permanent Entities');
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '.button--primary');
    // Nothing for edit or delete yet.
    $assert_session->elementNotExists('css', '.dropbutton .edit');
    $assert_session->elementNotExists('css', '.dropbutton .delete');
  }

  /**
   * Test that visiting the canonical route shows the page content.
   */
  public function testView() {
    $this->createEntityByCode('jupiter', 'Jupiter', 'planet');
    // Since full view mode is enabled, this should return code 200.
    $this->drupalGet('/permanent_entity/jupiter');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('Jupiter');
    $this->assertSession()->responseContains('permanent-entity');
    $this->assertSession()->responseContains('permanent-entity--view-mode-full');
    $this->assertSession()->responseContains('permanent-entity--type-planet');
    $this->assertSession()->responseContains('permanent-entity--id-jupiter');

    // Now disable the full view mode from the bundle settings.
    $entity_type = PermanentEntityType::load('planet');
    $entity_type->setfullModeAvailable(FALSE);
    $entity_type->save();
    $this->drupalGet('/permanent_entity/jupiter');
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Test that for an existent entity is only possible to edit it.
   */
  public function testOnlyEdit() {
    $this->createEntityByCode('jupiter', 'Jupiter', 'planet');

    $this->drupalGet(Url::fromRoute('entity.permanent_entity.collection'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('Jupiter');
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '.button--primary');
    $assert_session->elementNotExists('css', '.dropbutton .delete');
    $this->cssSelect("ul.dropbutton > li.dropbutton-action a:contains('Edit')");
    $this->drupalGet(
      Url::fromRoute(
        'entity.permanent_entity.edit_form',
        ['permanent_entity' => 'jupiter']
      )
    );

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('Jupiter');
    $save_button = $this->xpath('//input[@value="Save"]');
    $this->assertCount(1, $save_button, 'The Save button exists.');
    $this->assertSession()->responseNotContains('delete');
  }

  /**
   * Test that bundles cannot be deleted once there instances of it.
   */
  public function testBundleDeletion() {
    PermanentEntityType::create(['label' => 'Color', 'id' => 'color'])->save();
    $route = Url::fromRoute(
      'entity.permanent_entity_type.delete_form',
      ['permanent_entity_type' => 'color']
    );

    $this->drupalGet($route);
    $this->assertSession()->statusCodeEquals(200);

    // Create an instance and check that the bundle cannot be deleted anymore.
    $this->createEntityByCode('red', 'Red', 'color');
    $this->drupalGet($route);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Creates a permanent entity.
   *
   * @param string $id
   *   The id of the entity.
   * @param string $label
   *   The label of the entity.
   * @param string $type
   *   The type or bundle of the entity.
   */
  protected function createEntityByCode(string $id, string $label, string $type) {
    if (!PermanentEntityType::load($type)) {
      PermanentEntityType::create([
        'label' => $type,
        'id' => $type,
        'full_view_mode_available' => TRUE,
      ])->save();
    }

    PermanentEntity::create([
      'id' => $id,
      'label' => $label,
      'type' => $type,
    ])->save();
  }

}
